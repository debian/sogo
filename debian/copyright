Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: SOGo
Source: https://packages.sogo.nu/SOGo/sources/

Files: *
Copyright: 2000-2016 Inverse inc.
    2000-2007 SKYRIX Software AG
    2005-2007 Helge Hess
    2010 Wolfgang Sourdeau
    2011-2012 Jeroen Dekkers
License: GPL-3+

Files: ActiveSync/*
Copyright: 2013-2014 Inverse inc.
License: BSD-3-clause

Files: SOPE/NGCards/versitCardsSaxDriver/*
Copyright: 2003-2004 Max Berger
    2004-2005 OpenGroupware.org
License: LGPL-2+

Files: SOPE/NGCards/versitCardsSaxDriver/common.h
Copyright: 2004, OpenGroupware.org
License: LGPL-2+

Files: SoObjects/SOGo/NSData+Crypto.m
Copyright: 2012-2015, Inverse inc
  2012, Nicolas Höft
  2012, Jeroen Dekkers
License: GPL-2+

Files: SoObjects/SOGo/lmhash.c
  SoObjects/SOGo/lmhash.h
Copyright: 2004, Christopher R. Hertel
License: LGPL-2.1+

Files: UI/WebServerResources/js/vendor/ckeditor/*
Copyright: 2003-2016, CKSource - Frederico Knabben
License: GPL-2+ or MPL-1.1 or LGPL-2.1+

Files: UI/WebServerResources/scss/components/datepicker/*
Copyright: 2012 Stefan Petre
License: Apache-2.0

Files: UI/WebServerResources/js/vendor/angular*.js
Copyright: (C) 2010-2016 Google, Inc.
License: MIT

Files: UI/WebServerResources/js/vendor/lodash*.js
Copyright: Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
License: MIT

Files: debian/*
Copyright: 2011-2019 Jeroen Dekkers <jeroen@dekkers.ch>
           2016-2024 Jordi Mallach <jordi@debian.org>
License: GPL-2+

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache License, Version 2.0 can be
 found in "/usr/share/common-licenses/Apache-2.0".

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
     * Neither the name of the Inverse inc. nor the
       names of its contributors may be used to endorse or promote products
       derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-2+
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems the full text of the GNU General Public License can
 be found in the `/usr/share/common-licenses/GPL-2' file.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems the full text of the GNU General Public License can
 be found in the `/usr/share/common-licenses/GPL-3' file.

License: LGPL-2+
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems the full text of the GNU Lesser General Public
 License can be found in the `/usr/share/common-licenses/LGPL-2'
 file.

License: LGPL-2.1+
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published by the
 Free Software Foundation; version 2.1 of the License, or (at
 your option) any later version.
 .
 On Debian systems, the complete text of version 2.1 of the GNU Lesser
 General Public License can be found in `/usr/share/common-licenses/LGPL-2.1'.

License: MPL-1.1
 The contents of this file are subject to the Mozilla Public License
 Version 1.1 (the "License"); you may not use this file except in
 compliance with the License. You may obtain a copy of the License at
 http://www.mozilla.org/MPL/
 .
 Software distributed under the License is distributed on an "AS IS"
 basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 License for the specific language governing rights and limitations
 under the License.
 .
 On Debian systems, the full text of the MPL version 1.1 can be found in
 the `/usr/share/common-licenses/MPL-1.1' file.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
